//import
var jwt = require('jsonwebtoken');

const jWT_SECRET ='AZERTYUIO';
//exporter fonction
module.exports ={
    genererTokenUser: function(userData){
        return jwt.sign({
            userId: userData.id
        },
        jWT_SECRET,{
            expiresIn:'1h'
        })
    },
    parseAuthorization: function(authorization) {
        return (authorization != null) ? authorization.replace('Bearer ', '') : null;
    },
    getUserId: function(authorization) {
        var userId = -1;
        var token = module.exports.parseAuthorization(authorization);
        if(token != null) {
          try {
            var jwtToken = jwt.verify(token, jWT_SECRET);
            if(jwtToken != null)
              userId = jwtToken.userId;
          } catch(err) { }
        }
        return userId;
      }
}