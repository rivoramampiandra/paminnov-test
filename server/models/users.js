'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Users.hasMany(models.Livres);
    }
  };
  Users.init({
    prenom: DataTypes.STRING,
    nom: DataTypes.STRING,
    courriel: DataTypes.STRING,
    motPasse: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Users',
  });
  return Users;
};