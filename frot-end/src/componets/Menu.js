
import React,{useState} from 'react';
import {Link} from 'react-router-dom';



function Menu() {

  const [menuLivre, setMenuLivre] = useState('active');
  const [menuUser, setMenuUser] = useState('');
  
  const activeMenuUser = () =>{
    setMenuLivre("");
    setMenuUser("active");
  };
  const activeMenuLivre = () =>{
    setMenuLivre("active");
    setMenuUser("");
  };

  return (
    <div className="sidebar" data-color="purple" data-background-color="white">
      
      <div className="logo"> 
        <Link to="/acceuil" className="simple-text logo-normal">
          Gestion de livre
        </Link>
      </div>
      <div className="sidebar-wrapper">
        <ul className="nav">
          <li className={"nav-item "+menuLivre} onClick={activeMenuLivre}>
            <Link className="nav-link" to="/livre">
              <i className="material-icons">dashboard</i>
              <p>Livre</p>
            </Link>
          </li>
          <li className={"nav-item "+menuUser} onClick={activeMenuUser}>
            <Link to="/user" className="nav-link" >
              <i className="material-icons">person</i>
              <p>Utilisateur</p>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default Menu;
