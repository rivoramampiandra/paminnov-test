import React from 'react';
import {Switch, Route, Router} from 'react-router-dom';
import Livre from '../view/Livre';
import Users from '../view/Users';
import NewLivre from '../view/NewLivre';
import EditLivre from '../view/EditLivre';

function App() {
  return (
    <div className='content'>
      <Switch>
          <Route exact path="/acceuil" component={Livre}/>
          <Route  path="/livre" component={Livre}/>
          <Route  path="/user" component={Users}/>
          <Route  path="/newLivre" component={NewLivre}/>
          <Route  path="/editLivre/:id" component ={EditLivre}/>
      </Switch>
    </div>
      
  );
}

export default App;
