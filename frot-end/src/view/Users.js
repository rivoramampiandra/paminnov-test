
import React,{useState,useEffect} from 'react';
import Axios from 'axios';


function Users() {

  const [userList, setUserList] = useState([]);
  useEffect(() => {
      Axios.get("http://localhost:8080/api/users/listUser").then((response) => {
        setUserList(response.data);
        console.log(response);
      })
  },[]);

  return (
    <div className="container-fluid">
      <div className="card card-plain" style={{marginTop:'-70px'}}>
          <div className="card-header card-header-primary">
            <h4 className="card-title mt-0"> List des auteurs</h4>
          </div>
          <div className="card-body">
            <div className="divTable">
              <table className="table table-hover">
                <thead className="">
                  <tr>
                    <th>Nom </th>
                    <th>Prenom </th>
                    <th>Courriel </th>
                  </tr>
                </thead>
                <tbody>
                  {userList.map(function (val) {
                     return ( 
                      <tr key={val.id}> 
                        <td >{val.nom}</td> 
                        <td >{val.prenom}</td> 
                        <td > {val.courriel}</td>
                      </tr>
                    )
                  } )}
                  
                  
                  </tbody>
              </table>
            </div>
            

          </div>
        </div>
    </div>
  
  );
}

export default Users;
