
import React,{useState} from 'react';
import Axios from 'axios';
import {Link, useHistory} from 'react-router-dom';

function InitialPass() {

    const [courriel, setCourriel] = useState('');
    const [motPasse, setMotPasse] = useState('');
    const [motPasseConfir, setMotPasseConfir] = useState('');  

    const history = useHistory();

    const initialiserMotPasse =(event) => {

        //empêcher parametre par défaut 
        event.preventDefault();

        if(motPasse === motPasseConfir){
            Axios.put("http://localhost:8080/api/users/me", 
                {
                    courriel: courriel,
                    motPasse: motPasse
                }).then((response) => {
                    alert("Nouveau mots de passe enregistré avec succès!")
                    history.push('/login');
            }).catch(function (error) {
                if (error.response) {
                  alert(error.response.data.erreur)
                  console.log(error.response.data);
                } else if (error.request) {
                    alert(error.request)
                  console.log(error.request);
                } else {
                  alert(error.message);
                  console.log('Error', error.message);
                }
            });
        }else{
            alert("Votre mots des passes ne sont pas identiques");
        }
        
    };


  return (
    <div className="App">
        <div className= "row">
            <div className="col-md-6" style={{marginLeft:'380px',marginTop:'220px'}}>
                <div className="card">
                    <div className="card-header card-header-primary">
                        <h4 className="card-title">Réinitialisation du mots de passe</h4> 
                    </div>
                    <div className="card-body">
                        <form>
                        <div className="row">
                                <div className="col-md-10">
                                    <div className="form-group bmd-form-group">
                                        <label className="bmd-label-floating">Email:</label>
                                        <input type="text" className="form-control" name="courriel" onChange= {(e) => {setCourriel(e.target.value);}}  style={{paddingLeft:'120px'}}/>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-10">
                                    <div className="form-group bmd-form-group">
                                        <label className="bmd-label-floating">Mots de pass:</label>
                                        <input type="text" className="form-control" onChange= {(e) => {setMotPasse(e.target.value);}} name="motPasse" style={{paddingLeft:'120px'}}/>
                                    </div>
                                </div>
                            </div>
                            
                            <div className="row">
                                <div className="col-md-10">
                                    <div className="form-group bmd-form-group">
                                        <label className="bmd-label-floating">Confirmation: </label>
                                        <input type="password" className="form-control" onChange= {(e) => {setMotPasseConfir(e.target.value);}} name="motPasseConfir" style={{paddingLeft:'120px'}} />
                                    </div>
                                </div>
                            </div>
                            
                            <div className="row">
                                <div className="col-md-3">
                                    <button type="button" onClick={initialiserMotPasse} className="btn btn-primary pull-right">Valider</button>
                                </div>
                                <div className="col-md-4">

                                </div>
                                <div className="col-md-4">
                                    <Link to= "/login">
                                        <button  className="btn btn-secondary pull-right">Annuler</button>
                                    </Link>
                                    
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
         
    </div>
  );
}

export default InitialPass;
