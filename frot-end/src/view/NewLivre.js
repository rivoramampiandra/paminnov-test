
import React,{useState} from 'react';
import Axios from 'axios';
import {useHistory} from 'react-router-dom';
import Cookies from 'js-cookie';

//import Upload from '../Upload';

function NewLivre() {

    const [titre, setTitre] = useState('');
    const [description, setDescription] = useState('');
    const auteur = Cookies.get('userId');
    const [datePub, setDatePub] = useState('');
    var [type, setType] = useState('');
    const [couverture, setCouverture] = useState('');
    var [nameFile,setNameFile] = useState('');

    function fileSelect  (event) {
        
        let file = event.target.files[0];
        const fileSize = file.size;
        alert(file.name);
        if(fileSize < 92080){
             setNameFile(file.name);
            
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = (event) =>{
                console.warn(event.target.result);
                setCouverture(reader.result);
            }
        }
        else{
            event.target.value = '';
            alert("Fichier trop lourd pour encoder Base68!");
        }
        
    };

    const history = useHistory();
    //const couverture = null;

    const enregistrerLivre =(event) => {
        event.preventDefault();
        //var base64Couverture = couverture.split(',')[1];
        if(type ==="" || type === undefined || type.trim().length<0 ){
            type ="Public";
            
        }
     
            Axios.post("http://localhost:8080/api/livres/enregistrer", 
                {
                    titre: titre,
                    description: description,
                    id_user: auteur, 
                    datePub: datePub,
                    type: type,
                    couverture: couverture
                }).then((response) => {
                    alert("Enregistrement terminé!");
                    console.log(response.data);
                    history.push('/livre');
            }).catch(function (error) {
                if (error.response) {
                  alert(error.response.data.erreur)
                  console.log(error.response.data);
                } else if (error.request) {
                    alert(error.request)
                  console.log(error.request);
                } else {
                  alert(error.message);
                  console.log('Error', error.message);
                }
            });
   
    };


  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-12">
            <div className="card">
                <div className="card-header card-header-primary">
                    <h4 className="card-title">Ajout de livre</h4>
                </div>
                <div className="card-body">
                    <form>
                        <div className="row">
                            <div className="col-md-6">
                            <div className="form-group bmd-form-group">
                                <label for="titreId" >Titre:</label>
                                <input type="text" id="titreId" name="titre" onChange= {(e) => {setTitre(e.target.value);}} className="form-control"/>
                            </div>
                            </div>
                            <div className="col-md-6">
                            <div className="form-group bmd-form-group">
                                <label for="descriptionId">Description:</label>
                                <input type="text" id="descriptionId" className="form-control" name="description" onChange= {(e) => {setDescription(e.target.value);}}/>
                            </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                            <div className="form-group bmd-form-group">
                                <label for="dateId">Date de publication:</label>
                                <input type="date" id="dateId" className="form-control" name="datePub" onChange= {(e) => {setDatePub(e.target.value);}} />
                            </div>
                            </div>
                            <div className="col-md-6">
                            <div className="form-group bmd-form-group">
                                <label for="typeId">Type:</label>
                                <select name="type" class="form-control selectpicker" data-style="btn btn-link" id="exampleFormControlSelect1" onChange= {(e) => {setType(e.target.value);}}>
                                    <option value="Public" defaultValue>Public</option>
                                    <option value="Privée">Privée</option>
                                </select>
                            </div>
                            </div>
                        </div>
                        <div className="row">
                            
                            <div className="col-md-6">
                                <div className="custom-file">
                                    <label className="bmd-label-floating" style={{paddingRight: '10px'}}>Couverture: {nameFile} </label>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-fab btn-round btn-primary">
                                            <i class="material-icons">attach_file</i><input type="file" className="form-control inputFileVisible" id="customFileLangHTML"  onChange={fileSelect}  accept="/image/x-png,image/gif,image/jpeg" style={{opacity:0}}/>  
                                        </button>
                                    </span>

                                </div>
                         
                            </div>

                            <div className="col-md-6">
                            </div>
                        </div>
                        <button type="submit" className="btn btn-primary pull-right" onClick={enregistrerLivre} >Publier</button>
                        <div className="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
      </div>
    </div>
  );
}

export default NewLivre;