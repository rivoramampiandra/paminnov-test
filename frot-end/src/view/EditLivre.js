
import React,{useEffect, useState} from 'react';
import Axios from 'axios';
import {useHistory,useParams} from 'react-router-dom';

//import Upload from '../Upload';

function EditLivre() {

    let history = useHistory();
    const {id} = useParams();
    useEffect(()=>{
        loadLivre();
    },[]);

    const [titre, setTitre] = useState('');
    const [description, setDescription] = useState('');
    const [datePub, setDatePub] = useState('');
    const [type, setType] = useState('');
 
    const loadLivre = async () =>{
        const result = await Axios.get(`http://localhost:8080/api/livres/listOneLivre/${id}`);
      
        setTitre(result.data.titre);
        setDescription(result.data.description);
        setDatePub(result.data.datePub);
        setType(result.data.type);
    }

  
    
    const updateLivre =(event) => {
        event.preventDefault();
       
     
            Axios.put(`http://localhost:8080/api/livres/updateLivre/${id}`, 
            {
                titre: titre,
                description: description,
                datePub: datePub,
                type: type,
            }).then((response) => {
                    alert("Enregistrement terminé!");
                    console.log(response.data);
                    history.push('/livre');
            }).catch(function (error) {
                if (error.response) {
                  alert(error.response.data.erreur)
                  console.log(error.response.data);
                } else if (error.request) {
                    alert(error.request)
                  console.log(error.request);
                } else {
                  alert(error.message);
                  console.log('Error', error.message);
                }
            });
   
    };


  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-12">
            <div className="card">
                <div className="card-header card-header-primary">
                    <h4 className="card-title">Modification de livre</h4>
                </div>
                <div className="card-body">
                    <form>
                        <div className="row">
                            <div className="col-md-6">
                            <div className="form-group bmd-form-group">
                                <label for="titreId" >Titre:</label>
                                <input type="text" id="titreId" name="titre" value={titre}  onChange= {(e) => {setTitre(e.target.value);}} className="form-control"/>
                            </div>
                            </div>
                            <div className="col-md-6">
                            <div className="form-group bmd-form-group">
                                <label for="descriptionId">Description:</label>
                                <input type="text" id="descriptionId" className="form-control" value={description} name="description" onChange= {(e) => {setDescription(e.target.value);}}/>
                            </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                            <div className="form-group bmd-form-group">
                                <label for="dateId">Date de publication:</label>
                                <input type="date" id="dateId" className="form-control" value={datePub} name="datePub" onChange= {(e) => {setDatePub(e.target.value);}} />
                            </div>
                            </div>
                            <div className="col-md-6">
                            <div className="form-group bmd-form-group">
                                <label for="typeId">Type:</label>
                                <select name="type" class="form-control selectpicker" value={type} data-style="btn btn-link" id="exampleFormControlSelect1" onChange= {(e) => {setType(e.target.value);}}>
                                    <option value="Public" >Public</option>
                                    <option value="Privée">Privée</option>
                                </select>
                            </div>
                            </div>
                        </div>
                        
                        <button type="submit" className="btn btn-primary pull-right" onClick={updateLivre} >Modifier</button>
                       
                        <div className="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
      </div>
    </div>
  );
}

export default EditLivre;