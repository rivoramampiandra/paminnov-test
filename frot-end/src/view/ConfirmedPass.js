
import React from 'react';
import {Link, useHistory} from 'react-router-dom';

function ConfirmedPass() {

    const history = useHistory();

    

    //const [code, setCode] = useState('');

    function a () {
        const min = Math.ceil(1000);
        const max = Math.floor(9999);
        return Math.floor(Math.random() * (max - min +1)) + min;
        
    } 
    const autoCode = a();
    const ConfirmationMotPasse =(event) => {
        //empêcher parametre par défaut 
        event.preventDefault();
        const code =document.getElementById("code").value;

        if(autoCode == code){
            history.push('/initialPass');
        }  
    };


  return (
    <div className="App">
        <div className= "row">
            <div className="col-md-6" style={{marginLeft:'380px',marginTop:'220px'}}>
                <div className="card">
                    <div className="card-header card-header-primary">
                        <h4 className="card-title">Pour réinitialiser entre le code temporaire {autoCode}</h4> 
                    </div>
                    <div className="card-body">
                        <form>
                        
                            <div className="row">
                                <div className="col-md-10">
                                    <div className="form-group bmd-form-group">
                                        <label className="bmd-label-floating">Code:</label>
                                        <input type="text" className="form-control" id="code" style={{paddingLeft:'110px'}} />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3">
                                    <button type="submit" onClick={ConfirmationMotPasse} className="btn btn-primary pull-right">Confirmer</button>
                                </div>
                                <div className="col-md-4">

                                </div>
                                <div className="col-md-4">
                                    <Link to= "/login">
                                        <button  className="btn btn-secondary pull-right">Annuler</button>
                                    </Link>
                                    
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
         
    </div>
  );
}

export default ConfirmedPass;
