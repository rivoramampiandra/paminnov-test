
import React,{useState} from 'react';
import Axios from 'axios';
import {Link,  useHistory} from 'react-router-dom';
import Cookies from 'js-cookie';


function Login() {
    
    const [courriel, setCourriel] = useState('');
    const [motPasse, setMotsPasse] = useState('');
    
    const history = useHistory();

    //fonction s'authentifier
    const loginUser =(event) => {
        //empêcher parametre par défaut 
        event.preventDefault();

        //Requete au serveur api(userCtrl => login) pour s'identifier 
        Axios.post("http://localhost:8080/api/users/login", 
        {
            courriel: courriel, 
            motPasse: motPasse
        }).then((response)=> {
            console.log(response);
            history.push('/acceuil/');
            alert("Binvenu à Gestion de livre");
            Axios.get("http://localhost:8080/api/users/me",{headers: {
                'Authorization': 'Bearer '+response.data.token}
                }).then((response)=> {
                    console.log(response);
                    Cookies.set('courriel',response.data.courriel);
                    Cookies.set('userId',response.data.id);
                    Cookies.set('nom',response.data.nom);
                    Cookies.set('prenom',response.data.prenom);
                
                });
        }).catch(function (error) {
            if (error.response) {
              alert(error.response.data.erreur)
              console.log(error.response.data);
            } else if (error.request) {
                alert(error.request)
              console.log(error.request);
            } else {
              alert(error.message);
              console.log('Error', error.message);
            }
        });
    };


  return (
    <div className="App">
        <div className= "row">
            <div className="col-md-6" style={{marginLeft:'380px',marginTop:'220px'}}>
                <div className="card">
                    <div className="card-header card-header-primary">
                        <h1 className="card-title">Authentification</h1>
                      
                    </div>
                    <div className="card-body">
                        <form>
                            <div className="row">
                                <div className="col-md-10">
                                    <div className="form-group bmd-form-group">
                                        <label className="bmd-label-floating">Votre email:</label>
                                        <input type="text" className="form-control" name="courriel" onChange= {(e) => {setCourriel(e.target.value);}}  style={{paddingLeft:'120px'}}/>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-10">
                                    <div className="form-group bmd-form-group">
                                        <label className="bmd-label-floating">Mots de passe: </label>
                                        <input type="password" className="form-control" name="motPasse" style={{paddingLeft:'120px'}} onChange= {(e) => {setMotsPasse(e.target.value);}}/>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3">
                                    <button type="submit" onClick={loginUser} className="btn btn-primary pull-right">Connexion</button>
                                </div>  
                                <div className="col-md-4">
                                    <Link to="/confirmedPass" >Mots de passe oublier?</Link>
                                </div>
                                
                                <div className="col-md-4">
                                    <Link to ="/livrePub">
                                        <button type="button" className="btn btn-warning pull-right">Consulter Livre</button>
                                    </Link>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
         
    </div>
  );
}

export default Login;
